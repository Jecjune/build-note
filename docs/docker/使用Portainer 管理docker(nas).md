
1.通过putty拉取镜像源：
	[putty下载地址](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)  
	![docker安装1.png](pic/docker安装1.png)  
	先启动应用程序，然后通过ssh连接到群晖中：
	![docker安装2.png](pic/docker安装2.png)
	输入用户名和密码，开始使用putty。
	- 拉取源：
	![docker安装3.png](pic/docker安装3.png)  
    如果docker Hub拉不下来，就换一个，换成阿里云镜像源
    - 换源教程  
		- （1）去到阿里云控制台
		- ![docker安装4.png](pic/docker安装4.png)  
		- （2）点击进入镜像工具→镜像加速器能看到加速器地址，点击复制备用
		![docker安装5.png](pic/docker安装5.png)
	- （3）更改docker中的镜像源  
		- 1）点击进入docker→注册表→设置→选中Docker Hub点击编辑
		- ![docker安装6.png](pic/docker安装6.png)  
		- 2）勾选启用注册表镜像→将之前复制的网址粘贴进去→确认即可
		- ![docker安装7.png](pic/docker安装7.png)    
- 2.拉取成功后docker的列表里面会有显示
- ![docker安装8.png](pic/docker安装8.png)  
    
- 3.启动
- ![docker安装9.png](pic/docker安装9.png)  
    
- 4.这里选择使用与docker host相同的网络![docker安装10.png](pic/docker安装10.png)  
    
- 5.部署protainer  
    
    - Portainer 是 Docker 的图形化管理工具，提供状态显示面板、应用模板快速部署、容器镜像网络数据卷的基本操作（包括上传和下载镜像、创建容器等操作）、事件日志显示、容器控制台操作、Swarm 集群和服务等集中管理和操作、登录用户管理和控制等功能。  
    
    - （1）创建文件夹，在docker文件夹下新建protainer文件夹，在protainer文件夹下新建data文件夹用于挂载镜像文件的数据![docker安装11.png](pic/docker安装11.png)  
        
    
    - （2）打开ssh远程连接  
        
        - sudo -i  
            切换为root用户
        
        - 输入指令![docker安装12.png](pic/docker安装12.png)  
            docker run -d --restart=always --name="portainer" -p 任意端口:9000 -v /var/run/docker.sock:/var/run/docker.sock -v NAS本地实际路径:/data 6053537/portainer-ce  
              
            任意端口是NAS本地没有被占用的端口即可  
            NAS本地实际路径就是data文件夹所在的路径直接复制  
              
            最终指令： docker run -d --restart=always --name="portainer" -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v /volume1/docker/protainer/data:/data 6053537/portainer-ce
    
    - （3）容器运行中![docker安装13.png](pic/docker安装13.png)  
        
    
    - （4）protainer初始化  
        
        - 1.输入IP:9000（9000是刚刚安装的时候用的端口号）。然后自定义用户名和密码创建账户![docker安装14.png](pic/docker安装14.png)  
        - 2.选择管理本地正在运行的容器。![docker安装15.png](pic/docker安装15.png)  
            
        
        - 3.可以正确识别到本地容器列表，初始化完成。![docker安装16.png](pic/docker安装16.png)  
            
        
        - 4.查看容器![docker安装17.png](pic/docker安装17.png)  
            
        
        - 创建容器![docker安装18.png](pic/docker安装18.png)  
            填写名称：typcho  
            镜像：用户名/typcho:amd64  
            （格式：这里直接输入想要拉取的“用户名/镜像名:版本号”）
        
        - 正在进行部署容器![docker安装19.png](pic/docker安装19.png)
    