# 一键部署个人网站

添加站点![一键部署个人网站1.png](../picture/一键部署个人网站1.png)
一键部署
![一键部署个人网站2.png](../picture/一键部署个人网站2.png)
创建成功后请记住数据库名、用户、密码![一键部署个人网站3.png](../picture/一键部署个人网站3.png)
然后点击访问站点后填写数据库连接信息，把刚刚的数据库名、用户名、密码复制过来就好，其他的不用改。然后提交![一键部署个人网站4.png](../picture/一键部署个人网站4.png)
然后根据自己喜好填写设置![一键部署个人网站5.png](../picture/一键部署个人网站5.png)

 启动Nginx服务
```
 sudo systemctl start nginx  
```
启动MySQL服务：
```
sudo systemctl start mysqld  
```
开机自启：
```
​systemctl enable mysqld
```
 查看状态：
```
 systemctl status mysqld  
```
 启动Pure-FTPd服务：
```
 sudo systemctl start pure-ftpd  
```
 出现这种报错八成是80端口被占用  
```
sudo systemctl start nginx  
Job for nginx.service failed because the control process exited with error code.See "systemctl status nginx.service" and "journalctl -xe" for details.  
```
查看哪个进程占用了80端口：
```
sudo lsof -i :80  
```
杀死对应PID的进程
```
kill PID
```
### Typcho安装
添加站点
![一键部署个人网站6.png](../picture/一键部署个人网站6.png)
添加网站
![一键部署个人网站7.png](../picture/一键部署个人网站7.png)
添加成功
![一键部署个人网站8.png](../picture/一键部署个人网站8.png)
点进域名设置，这个可关可不关，然后部署证书
![一键部署个人网站9.png](../picture/一键部署个人网站9.png)
![一键部署个人网站10.png](../picture/一键部署个人网站10.png)
去Typecho官网下载文件:[https://typecho.org/](https://cloud.tencent.com/developer/tools/blog-entry?target=https%3A%2F%2Ftypecho.org%2F&source=article&objectId=1667724)
![一键部署个人网站11.png](../picture/一键部署个人网站11.png)
![一键部署个人网站12.png](../picture/一键部署个人网站12.png)
在网站根目录下上传压缩文件，然后解压
![一键部署个人网站13.png](../picture/一键部署个人网站13.png)
解压后图示
![一键部署个人网站14.png](../picture/一键部署个人网站14.png)
最后浏览器访问 你的网址/install.php，按提示完成 Typecho的相关配置。
![一键部署个人网站15.png](../picture/一键部署个人网站15.png)
初始化配置的时候，不知道数据库数据可以去宝塔查一下
![一键部署个人网站16.png](../picture/一键部署个人网站16.png)
![一键部署个人网站17.png](../picture/一键部署个人网站17.png)
![一键部署个人网站18.png](../picture/一键部署个人网站18.png)
![一键部署个人网站19.png](../picture/一键部署个人网站19.png)
域名可以直接访问网站
![一键部署个人网站20.png](../picture/一键部署个人网站20.png)
输入域名/admin，可以访问控制台：
![一键部署个人网站21.png](../picture/一键部署个人网站21.png)