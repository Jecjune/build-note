# gitlab邮箱通知

进入群晖dsm管理页面，找到docker插件，在docker插件中找到`gitlab`容器，然后起一个控制中断，然后编辑`/etc/gitlab/gitlab.rb`，在配置文件找到类似下面的代码段落：
```
 gitlab_rails['smtp_enable'] = true
 gitlab_rails['smtp_address'] = "smtp.qq.com"
 gitlab_rails['smtp_port'] = 465
 gitlab_rails['smtp_user_name'] = "___"
 gitlab_rails[''] = "____"
 gitlab_rails['smtp_domain'] = "smtp.qq.com"
 gitlab_rails['smtp_authentication'] = "login"
 gitlab_rails['smtp_enable_starttls_auto'] = false
 gitlab_rails['smtp_tls'] = true
 gitlab_rails['smtp_openssl_verify_mode'] = 'none'
 gitlab_rails['smtp_pool'] = false
 gitlab_rails['gitlab_email_enabled'] = true
 gitlab_rails['gitlab_email_from'] = '___'
```

上述代码段是使用qq邮箱的示例，照着在配置文件里填入你准备好的邮箱信息即可：

`smtp_user_name`：用户名，这里填你邮箱的邮箱号`xx@qq.com`
`smtp_password`：密钥，这里不是你的登陆qq邮箱的密码，是你在qq邮箱的设置里开启IMAP/SMTP服务时，qq邮箱提供的认证码。
`gitlab_email_from`：发信人，这里建议和`smtp_user_name`填一样的，因为有的教程说不填一样的会报错。

其他的配置照着写就好了，都是固定的，感兴趣可以自行了解有什么作用。